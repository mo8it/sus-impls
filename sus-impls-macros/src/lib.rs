use proc_macro::TokenStream;
use quote::{format_ident, quote};
use syn::{parse::Parser, Error};

fn inner_test_impls(input: TokenStream) -> Result<proc_macro2::TokenStream, Error> {
    let parser = syn::punctuated::Punctuated::<syn::Expr, syn::token::Comma>::parse_terminated;
    let input = parser.parse(input)?;
    let mut args = input.iter();

    let Some(syn::Expr::Array(names)) = args.next()
    else {
        return Err(Error::new_spanned(input, "Wrong input!"));
    };

    let Some(syn::Expr::Lit(syn::ExprLit {lit: syn::Lit::Str(input_str), ..})) = args.next()
    else {
        return Err(Error::new_spanned(input, "Wrong input!"));
    };
    let input_str = input_str.value();

    let Some(syn::Expr::Array(expected)) = args.next()
    else {
        return Err(Error::new_spanned(input, "Wrong input!"));
    };

    let unstringified_names = names
        .elems
        .iter()
        .map(|e| {
            let syn::Expr::Lit(syn::ExprLit{lit: syn::Lit::Str(e), ..}) = e
            else {
                panic!();
            };

            format_ident!("{}", e.value())
        })
        .collect::<Vec<_>>();

    let impls = expected.elems.iter().map(|expected| {
        let syn::Expr::Array(expected) = expected
        else {
            panic!();
        };

        let expected = expected
            .elems
            .iter()
            .map(|e| {
                let syn::Expr::Path(path) = e
            else {
                panic!()
            };

                path.path.segments.first().unwrap().ident.to_string()
            })
            .collect::<Vec<_>>();

        let generics =
            expected
                .iter()
                .zip(unstringified_names.iter())
                .filter_map(|(expected, name)| {
                    if expected == "Generic" {
                        Some(name)
                    } else {
                        None
                    }
                });

        let trait_generics = expected
            .iter()
            .zip(unstringified_names.iter().cloned())
            .map(|(expected, name)| {
                if expected == "Generic" {
                    name
                } else {
                    format_ident!("{}", expected)
                }
            });

        quote! {
            impl<#(#generics),*> Tr<#(#trait_generics),*> for T {}
        }
    });

    Ok(quote! {
        {
            use Dependency::*;

            test_expected(#names, #input_str, #expected);
        }

        trait Tr<#(#unstringified_names),*> {}

        struct Set;
        struct Unset;

        struct T;

        #(#impls)*
    })
}

#[proc_macro]
pub fn test_impls(input: TokenStream) -> TokenStream {
    match inner_test_impls(input) {
        Ok(output) => output.into(),
        Err(error) => error.to_compile_error().into(),
    }
}
