# Publish on crates.io
publish:
	cargo outdated --exit-code 1
	typos
	cargo test --workspace
	cargo publish

	git tag -a -m "release" "v$(cargo read-manifest | jaq -r '.version')"
	git push --follow-tags origin main
